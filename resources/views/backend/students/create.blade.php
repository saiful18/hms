<x-backend.layouts.master>
    <form action="{{ route('students.store') }}" class="row g-3 w-75 mt-2 mx-auto p-5" method="post"
        style="background-color: #f1f1f1">
        @CSRF
        <div class="col-md-12 mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
        <div class="col-12 mb-3">
            <label for="address" class="form-label">Address</label>
            <input type="text" class="form-control" id="address" name="address">
        </div>

        <div class="col-md-6 mb-3">
            <label for="phone" class="form-label">Phone</label>
            <input type="text" class="form-control" id="phone" name="phone">
        </div>
        <div class="col-md-6 mb-3">
            <label for="dob" class="form-label">Date of Birth</label>
            <input type="date" class="form-control" id="dob" name="dob">
        </div>
        <div class="col-md-2 mb-3">
            <label for="gender" class="form-label">Gender</label>
        </div>
        <div class="col-md-2 mb-3">
            <input class="form-check-input" type="radio" name="gender" id="gender" value="Male" checked>
            <label class="form-check-label" for="malegenderLabel">
                Male
            </label>
        </div>
        <div class="col-md-2 mb-3">
            <input class="form-check-input" type="radio" name="gender" id="gender" value="Female">
            <label class="form-check-label" for="femalegenderLabel">
                Female
            </label>
        </div>
        {{-- <div class="col-md-2 mb-3">

        </div> --}}
        <div class="col-md-2 mb-3">
            <input class="form-check-input" type="radio" name="gender" id="gender" value="Others">
            <label class="form-check-label" for="othersgenderLabel">
                Others
            </label>
        </div>
        <div class="col-md-2 mb-3">
            <input class="form-check-input" type="radio" name="gender" id="gender" value="Custom">
            <label class="form-check-label" for="customgenderLabel">
                Custom
            </label>
        </div>
        <div class="col-12">
            <button type="submit" class="btn btn-outline-secondary">Create</button>
        </div>
    </form>
</x-backend.layouts.master>

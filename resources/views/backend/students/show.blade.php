<x-backend.layouts.master>
    {{-- {{ dd($studentData) }} --}}
    <div class="container-fluid" style="background-color: #f1f1f1">
        <div class=" mt-2 mx-auto p-5 text-left w-50">
            <h3><b>Student Name:</b> {{ $studentData->name }}</h3>
            <h5><b>Address:</b> {{ $studentData->address }}</h5>
            <h5><b>Phone:</b> {{ $studentData->phone }}</h5>
            <h5><b>Date Of Birth:</b> {{ $studentData->dob }}</h5>
            <h5><b>Gender:</b> {{ $studentData->gender }}</h5>
        </div>
    </div>
</x-backend.layouts.master>

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\StudentController;
//use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
// Route::get( '/', function () {
//     return view( 'backend.students.dashboard' );
// } );

// Route::get( '/', [StudentController::class, 'dashboard'] )->name( 'students.dashboard' );
// Route::get( '/students/create', [StudentController::class, 'create'] )->name( 'students.add' );
// Route::post( '/students/store', [StudentController::class, 'store'] )->name( 'students.store' );
// Route::get( '/students/show/{st }', [StudentController::class, 'show'] )->name( 'students.show' );
// Route::get( '/students/{studentData}/edit', [StudentController::class, 'edit'] )->name( 'students.edit' );
// Route::patch( '/students/update/{studentData}', [StudentController::class, 'update'] )->name( 'students.update' );
// Route::delete( '/students/{studentData}/delete', [StudentController::class, 'delete'] )->name( 'students.delete' );

Route::controller( StudentController::class )->group( function () {
    Route::get( '/students', 'dashboard' )->name( 'students.dashboard' );
    Route::get( '/students/create', 'create' )->name( 'students.add' );
    Route::post( '/students/store', 'store' )->name( 'students.store' );
    Route::get( '/students/show/{studentData}', 'show' )->name( 'students.show' );
    Route::get( '/students/{studentData}/edit', 'edit' )->name( 'students.edit' );
    Route::patch( '/students/update/{studentData}', 'update' )->name( 'students.update' );
    Route::delete( '/students/{studentData}/delete', 'delete' )->name( 'students.delete' );

} );

// Route::get( '/index', function () {
//     return view( 'index' );
// } );

// Route::get( '/dashboard', function () {
//     return view( 'dashboard' );
// } );

Route::controller( ImageController::class )->group( function () {
    Route::get( '/image', 'index' )->name( 'image.index' );
    Route::post( '/image', 'imageUpload' )->name( 'image.upload' );
} );


Route::get('/', function () {
    return view('welcome');
});
